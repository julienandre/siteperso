'use strict';

/**
 * @ngdoc function
 * @name showcaseApp.controller:Exp_proCtrl
 * @description
 * # Exp_proCtrl
 * Controller of the showcaseApp
 */
angular.module('showcaseApp')
  .controller('Exp_proCtrl', function ($scope,$http) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
    $http.get('/data/data.json').success(function(data){
        $scope.data = data.data;
      }
    )
  });
