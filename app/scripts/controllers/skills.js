'use strict';

/**
 * @ngdoc function
 * @name showcaseApp.controller:SkillsCtrl
 * @description
 * # SkillsCtrl
 * Controller of the showcaseApp
 */
angular.module('showcaseApp')
  .controller('SkillsCtrl', function ($scope,$http) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
    $http.get('/data/data.json').success(function(data){
        $scope.data = data.data;
      }
    )
  });
